import numpy as np
# import scipy as sp

items = []

def generate():
    for i in range(10):
        item = np.random.uniform(-1,1, size = 5)        # Генерим первые 5 чисел
        item = np.around(item, decimals=7)              # Округляем до 7 знаков после запятой

        generBin = np.random.randint(2, size = 1)       # Генерим шестое число 0 или 1
        res = np.append(item, generBin)                 # Добавляем в текущеим 5ти сгенерированное шестое числа

        items.append(res)                               # Добавляем всё в общий массив
    return items





generate()

print(items)
